package pl.edu.uwm.wmii.kruczekkamil.laboratorium08;

import pl.imiajd.Kruczek.*;
import java.util.Arrays;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba [] grupa = new Osoba[5];
        grupa[0] = new Osoba("Kowalski","1980-09-16");
        grupa[1] = new Osoba("Kowalski","1970-01-16");
        grupa[2] = new Osoba("Kacprzak","1965-09-15");
        grupa[3] = new Osoba("Antoniuk","1970-01-16");
        grupa[4] = new Osoba("Kruczek","1995-06-20");
        
        for(Osoba o: grupa)
        {
            System.out.println(o.toString());
        }
        
        Arrays.sort(grupa);
        System.out.println();
        for(Osoba o: grupa)
        {
            System.out.print(o.toString());
            System.out.println(" Lat: " + o.ileLat() + ", Miesiecy: "+ o.ileMiesiecy() + ", Dni: " + o.ileDni());
        }


    }
}



