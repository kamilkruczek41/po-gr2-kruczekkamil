package pl.imiajd.Kruczek;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>
{
    public Student (String nazwisko, String dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    @Override
    public String toString() {
        return "Student[" + " Nazwisko: " + nazwisko + " Data urodzenia: " + this.getDataUrodzenia() + " Srednia ocen: " + sredniaOcen + ']';
    }

    public int compareTo(Student obiekt)
    {
        return this.compareTo(obiekt);
    }

    public double getSredniaOcen()
    {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen)
    {
        this.sredniaOcen=sredniaOcen;
    }

    private double sredniaOcen;
}