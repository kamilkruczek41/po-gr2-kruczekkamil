package pl.imiajd.Kruczek;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Osoba implements Cloneable, Comparable<Osoba>
{
    public Osoba(String nazwisko, String dataUrodzenia)
    {
        this.nazwisko=nazwisko;
        this.dataUrodzenia=LocalDate.parse(dataUrodzenia, formatter);
    }
    
    public String toString()
    {
        return "Osoba [" + nazwisko + " " + dataUrodzenia.format(formatter) + "]";
    }
    
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Osoba other = (Osoba) obj;
        if (!Objects.equals(this.nazwisko, other.nazwisko)) {
            return false;
        }
        if (!Objects.equals(this.dataUrodzenia, other.dataUrodzenia)) {
            return false;
        }
        return true;
    }
    
    public int compareTo(Osoba obiekt)
    {
        if (nazwisko.compareTo(obiekt.nazwisko)==0)
            return dataUrodzenia.format(formatter).compareTo(obiekt.dataUrodzenia.format(formatter));
        else
            return nazwisko.compareTo(obiekt.nazwisko);
    }
    
    public String getDataUrodzenia()
    {
        return dataUrodzenia.format(formatter);
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
