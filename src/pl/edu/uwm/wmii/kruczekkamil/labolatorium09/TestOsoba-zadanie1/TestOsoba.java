package pl.edu.uwm.wmii.kruczekkamil.labolatorium09;

import pl.imiajd.Kruczek.*;
import java.util.Arrays;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba [] grupa = new Osoba[5];
        grupa[0] = new Osoba("Kowal","1980-09-16");
        grupa[1] = new Osoba("Kowal","1970-01-16");
        grupa[2] = new Osoba("Kacprzak","1965-01-20");
        grupa[3] = new Osoba("Antoniuk","1965-01-20");
        grupa[4] = new Osoba("Kruczek","1995-06-20");
        
        for(Osoba o: grupa)
        {
            System.out.println(o.toString());
        }
        
        Arrays.sort(grupa);
        System.out.println();
        for(Osoba o: grupa)
        {
            System.out.print(o.toString() + "\n");
        } 
    }
}



