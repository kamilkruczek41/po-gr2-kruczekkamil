package pl.edu.uwm.wmii.kruczekkamil.labolatorium09;

import pl.imiajd.Kruczek.*;
import java.util.Arrays;

public class TestNazwanyPunkt
{
    public static void main(String[] args)
    {
        Student [] grupa = new Student[5];
        grupa[0] = new Student("Kowal","1995-01-16",3.65);
        grupa[1] = new Student("Kowalski","1995-01-16",4.05);
        grupa[2] = new Student("Antoniuk","1997-01-20",3.5);
        grupa[3] = new Student("Antoniuk","1998-01-22",4.56);
        grupa[4] = new Student("Kruczek","1995-06-20",3.45);

        for(Osoba o: grupa)
        {
            System.out.println(o.toString());
        }

        Arrays.sort(grupa);
        System.out.println();
        for(Osoba o: grupa)
        {
            System.out.println(o.toString());
        }
    }
}



