package pl.imiajd.Kruczek;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Osoba
{
    public Osoba(String imiona, String nazwisko, String dataUrodzenia, boolean plec)
    {
        this.nazwisko=nazwisko;
        this.dataUrodzenia=LocalDate.parse(dataUrodzenia, formatter);
        this.imiona=imiona;
        this.plec=plec;
    }
    
    public abstract String getOpis();
    
    public boolean getPlec()
    {
        return plec;
    }
    
    public String getImiona()
    {
        return imiona;
    }
    
    public String getDataUrodzenia()
    {
        return dataUrodzenia.format(formatter);
    }
    
    public String getNazwisko()
    {
        return nazwisko;
    }
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected String nazwisko;
    protected String imiona;
    protected LocalDate dataUrodzenia;
    protected boolean plec;
}
