package pl.imiajd.Kruczek;

import java.time.LocalDate;

public class Pracownik extends Osoba
{
    public Pracownik(String imiona, String nazwisko, String dataUrodzenia, String dataZatrudnienia, boolean plec, double pobory)
    {
        super(imiona,nazwisko,dataUrodzenia,plec);
        this.pobory=pobory;
        this.dataZatrudnienia=LocalDate.parse(dataZatrudnienia, formatter);
    }
    public double getPobory()
    {
        return pobory;
    }
    
    public String getDataZatrudnienia()
    {
        return dataZatrudnienia.format(formatter);
    }
    
    public String getOpis()
    {
        return String.format("pracownik z pensja %.2f zl", pobory);
    }
    
    private double pobory;
    private LocalDate dataZatrudnienia;
}
