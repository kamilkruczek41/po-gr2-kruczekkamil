package pl.imiajd.Kruczek;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Instrument {
    
    public Instrument(String producent, String rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = LocalDate.parse(rokProdukcji, formatter);
    }
    
    public abstract String dzwiek();
    
    public String getProducent()
    {
        return producent;
    }
    
    public String getRokProdukcji()
    {
        return rokProdukcji.format(formatter);
    }
    
    public boolean equals(Object instrument)
    {
        if (instrument == null)
            return false;
        if (!(instrument instanceof Instrument))
            return false;
        Instrument inst = (Instrument) instrument;
        return (inst.producent == producent && inst.rokProdukcji.format(formatter).equals(rokProdukcji.format(formatter)));
    }
    
    public String toString()
    {
        return "Producent: " + producent + "\nRok produkcji: " + rokProdukcji.format(formatter) + "\n";
    }
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    String producent;
    LocalDate rokProdukcji;
}
