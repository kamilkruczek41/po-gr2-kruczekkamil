package pl.edu.uwm.wmii.kruczekkamil.laboratorium07;

import pl.imiajd.Kruczek.*;

public class TestInstrument
{
    public static void main(String[] args)
    {
        ArrayList <Instrument> orkiestra = new ArrayList <Instrument>();
        Flet flet1 = new Flet("Fiufiu","2017-09-18");
        Skrzypce skrzypce1 = new Skrzypce("SkrzypuSkrzypu","2018-01-09");
        Fortepian fortepian1 = new Fortepian("Chopin","1812-01-09");
        Flet flet2 = new Flet("FiuuFiuu","2013-02-29");
        Skrzypce skrzypce2 = new Skrzypce("Vivaldi","1756-09-12");

        orkiestra.add(flet1);
        orkiestra.add(skrzypce1);
        orkiestra.add(fortepian1);
        orkiestra.add(skrzypce2);
        orkiestra.add(flet2);

        System.out.println(flet1.dzwiek());
        System.out.println(fortepian1.dzwiek());
        System.out.println(skrzypce1.dzwiek());
        for(Instrument k: orkiestra)
        {
            System.out.println(k.toString());
        }
    }
}