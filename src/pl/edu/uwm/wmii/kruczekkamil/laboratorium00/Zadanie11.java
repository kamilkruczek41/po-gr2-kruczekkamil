package pl.edu.uwm.wmii.kruczekkamil.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("         \"Trzy słowa najdziwniejsze\"");
        System.out.print("\n");
        System.out.println("         Kiedy wymawiam słowo Przyszłość,");
        System.out.println("         pierwsza sylaba odchodzi już do przeszłości. ");
        System.out.print("\n");
        System.out.println("         Kiedy wymawiam słowo Cisza, ");
        System.out.println("         niszczę ją.");
        System.out.print("\n");
        System.out.println("         Kiedy wymawiam słowo Nic,");
        System.out.println("         stwarzam co, co nie mieści się w żadnym niebycie");
    }
}