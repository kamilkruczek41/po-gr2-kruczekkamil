package pl.edu.uwm.wmii.kruczekkamil.laboratorium00;

public class Zadanie7 {
    public static void main(String[] args) {
        System.out.println("*     *                                   *     *                                          ");
        System.out.println("*    *                       *  *         *    *                                           ");
        System.out.println("*   *                           *         *   *                                         *   ");
        System.out.println("*  *      ****   * *** ***   *  *         *  *     * ***    *   *   ***   *****   ***   *   ");
        System.out.println("* *           *  **   *   *  *  *         * *      **    *  *   *  *   *     *   *   *  *  * ");
        System.out.println("*  *      *****  *    *   *  *  *         *  *     *        *   *  *        *    *****  * *  ");
        System.out.println("*   *    *    *  *    *   *  *  *         *   *    *        *   *  *       *     *      **   ");
        System.out.println("*    *   *   **  *    *   *  *  *   *     *    *   *        *   *  *   *  *      *   *  * *  ");
        System.out.println("*     *   *** *  *    *   *  *   ***      *     *  *         ***    ***   *****   ***   *  * ");
    }
}
