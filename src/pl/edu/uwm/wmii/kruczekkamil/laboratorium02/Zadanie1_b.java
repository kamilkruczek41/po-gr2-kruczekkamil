package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Zadanie1_b {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, ujemne=0, dodatnie=0, zero=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        Random liczb = new Random();
        ArrayList<Integer> list=new ArrayList<Integer>();
        if (ile >= 1 && ile <=100) {
            for (int i = 0; i < ile; i++) {
                liczba = liczb.nextInt(999 - (-999) + 1) + (-999);
                list.add(liczba);
            }

            for (int n : list) {
                if (n < 0)
                    ujemne++;
                else if (n == 0)
                    zero++;
                else
                    dodatnie++;
            }
            System.out.print(list + "\n Dodatnie: " + dodatnie + "\n Zera: " + zero + "\n Ujemne: " + ujemne);
        }
        else
            System.out.println("1 <= n <= 100 !!!");
    }

}