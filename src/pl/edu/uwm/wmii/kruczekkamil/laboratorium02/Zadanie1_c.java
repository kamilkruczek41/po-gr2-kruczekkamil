package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Zadanie1_c {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, max, ile_max=1;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        Random liczb = new Random();
        ArrayList<Integer> list=new ArrayList<Integer>();
        if (ile >= 1 && ile <=100) {
            for (int i = 0; i < ile; i++) {
                liczba = liczb.nextInt(999 - (-999) + 1) + (-999);
                list.add(liczba);
            }

            max = list.get(0);

            for (int n : list) {
                if (n > max) {
                    max = n;
                    ile_max=1;
                }
                else if (max==n)
                    ile_max++;

            }
            System.out.print(list + "\n Max: " + max + "\n Ile razy wystapilo: " + ile_max);
        }
        else
            System.out.println("1 <= n <= 100 !!!");
    }

}