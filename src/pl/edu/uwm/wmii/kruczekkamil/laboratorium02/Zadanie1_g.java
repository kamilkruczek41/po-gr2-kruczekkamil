package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Zadanie1_g {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, max = 0, lewy, prawy, tmp;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        Random liczb = new Random();
        ArrayList<Integer> list = new ArrayList<Integer>();
        if (ile >= 1 && ile <= 100) {
            for (int i = 0; i < ile; i++) {
                liczba = liczb.nextInt(999 - (-999) + 1) + (-999);
                list.add(liczba);
            }
            System.out.print(list);

            System.out.print("\nPodaj lewy kraniec: ");
            lewy = odczyt.nextInt();
            System.out.print("Podaj prawy kraniec: ");
            prawy = odczyt.nextInt();

            for (int i=lewy, j=0; i<=(prawy-lewy)/2; i++, j++)
            {
                tmp=list.get(lewy+i);
                list.set(i, list.get(prawy-j));
                list.set(prawy-j,tmp);
            }
            System.out.print(list);
        }
        else
            System.out.println("1 <= n <= 100 !!!");
    }

}