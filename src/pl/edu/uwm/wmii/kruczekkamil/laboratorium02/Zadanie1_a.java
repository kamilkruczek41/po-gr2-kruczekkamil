package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Zadanie1_a {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, parzyste=0, nieparzyste=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        Random liczb = new Random();
        ArrayList<Integer> list=new ArrayList<Integer>();
        for (int i=0;i<ile;i++)
        {
            liczba=liczb.nextInt(999 -(-999)+1) + (-999);
            list.add(liczba);
        }

        for (int n : list)
        {
            if(n%2==0)
                parzyste++;
            else
                nieparzyste++;
        }
        System.out.print(list + "\n Parzyste: " + parzyste + "\n Nieparzyste: " + nieparzyste);
    }

}