package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class Zadanie1_e {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, fragment=0, max=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        Random liczb = new Random();
        ArrayList<Integer> list=new ArrayList<Integer>();
        if (ile >= 1 && ile <=100) {
            for (int i = 0; i < ile; i++) {
                liczba = liczb.nextInt(999 - (-999) + 1) + (-999);
                list.add(liczba);
            }

            for (int n : list) {
                if (n>0)
                    fragment++;
                else {
                    if (fragment>max)
                        max = fragment;
                    fragment=0;
                }
            }
            System.out.print(list + "\n Najdluzszy fragment liczb dodatnich: " + max);
        }
        else
            System.out.println("1 <= n <= 100 !!!");
    }

}