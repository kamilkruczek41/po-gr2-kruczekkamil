package pl.edu.uwm.wmii.kruczekkamil.labolatorium04;
import  java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();

        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(14);
        a.add(5);
        a.add(3);
        a.add(12);

        System.out.println(reversed(a));
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> b = new ArrayList<Integer>();

        for (int i=0;i<a.size();i++)
        {
            b.add(a.get(a.size()-1-i));
        }
        return b;
    }
}
