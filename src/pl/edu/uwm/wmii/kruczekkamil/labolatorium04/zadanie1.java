package pl.edu.uwm.wmii.kruczekkamil.labolatorium04;
import java.util.ArrayList;

public class zadanie1 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();

        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println(append(a,b));
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a , ArrayList<Integer> b){
        for(int i=0; i<b.size();i++)
            a.add(b.get(i));
        return a;
    }
}
