package pl.edu.uwm.wmii.kruczekkamil.labolatorium04;
import java.util.ArrayList;

public class zadanie2 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();

        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(14);
        a.add(5);
        a.add(3);
        a.add(12);

        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        b.add(12);
        System.out.println(merge(a,b));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a , ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<Integer>();
        int ile;

        if (a.size()>b.size())
            ile = a.size() - Math.abs(a.size()-b.size());
        else if (b.size()>a.size())
            ile = b.size() - Math.abs(a.size()-b.size());
        else
            ile = a.size();

        for(int i=0; i<ile;i++) {
            c.add(a.get(i));
            c.add(b.get(i));
        }

        if (a.size()>b.size())
        {
            for(int i=a.size()-b.size();i>0;i--)
                c.add(a.get(a.size()-i));
        }
        else if (a.size()<b.size())
        {
            for(int i=b.size()-a.size();i>0;i--)
                c.add(b.get(b.size()-i));
        }
        return c;
    }
}
