package pl.edu.uwm.wmii.kruczekkamil.laboratorium03;
import java.util.Scanner;

public class zadanie1_b {

    public static void main(String[] args) {
        System.out.print("Podaj napis: ");
        Scanner wczytaj = new Scanner(System.in);
        String napis = wczytaj.nextLine();
        System.out.print("Podaj podnapis: ");
        String napis1 = wczytaj.nextLine();
        System.out.print("Podnapis wystapil: " + countSubStr(napis,napis1));
    }

    public static int countSubStr(String napis,String napis1)
    {
        int ile=0;
        for (int i=0; i<napis.length();i++)
        {
            if (napis.startsWith(napis1,i))
            {
                ile++;
            }
        }
        return ile;
    }
}