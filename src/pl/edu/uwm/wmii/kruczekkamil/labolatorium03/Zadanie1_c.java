package pl.edu.uwm.wmii.kruczekkamil.laboratorium03;
import java.util.Scanner;

public class Zadanie1_c {

    public static void main(String[] args) {
        System.out.print("Podaj napis: ");
        Scanner wczytaj = new Scanner(System.in);
        String napis = wczytaj.nextLine();
        System.out.print("Srodek: " + middle(napis));
    }

    public static String middle(String napis)
    {
        String srodek;
        if(napis.length()%2==0)
        {
            srodek = napis.substring(napis.length()/2-1, napis.length()/2+1);
        }
        else
        {
            srodek = napis.substring(napis.length()/2, napis.length()/2+1);
        }
        return srodek;
    }
}