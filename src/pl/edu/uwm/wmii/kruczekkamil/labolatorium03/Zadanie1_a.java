package pl.edu.uwm.wmii.kruczekkamil.laboratorium03;
import java.util.Scanner;

public class Zadanie1_a {

    public static void main(String[] args) {
        System.out.print("Podaj napis: ");
        Scanner wczytaj = new Scanner(System.in);
        String napis = wczytaj.nextLine();
        System.out.print("'c' wystapilo: " + countChar(napis,'c'));
    }

    public static int countChar(String napis, char c)
    {
        int ile=0;
        for (int i=0; i<napis.length();i++)
        {
            if (c==napis.charAt(i))
            {
                ile++;
            }
        }
        return ile;
    }
}