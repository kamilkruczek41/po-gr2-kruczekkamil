package pl.edu.uwm.wmii.kruczekkamil.laboratorium03;
import java.util.Scanner;

public class Zadanie1_d {

    public static void main(String[] args) {
        System.out.print("Podaj napis: ");
        Scanner wczytaj = new Scanner(System.in);
        String napis = wczytaj.nextLine();
        System.out.print("Podaj ile razy: ");
        int ile = wczytaj.nextInt();
        System.out.print("Napis: " + repeat(napis,ile));
    }

    public static String repeat(String napis, int n)
    {
        String dlugi_napis=napis;
        for (int i=1;i<n;i++)
        {
            dlugi_napis = dlugi_napis.concat(napis);
        }
        return dlugi_napis;
    }
}