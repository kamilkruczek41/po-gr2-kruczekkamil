package pl.edu.uwm.wmii.kruczekkamil.laboratorium02;

import java.util.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Zadanie3
{
    public static void main(String[] args)
    {
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000, "2001-12-15");
        personel[1] = new Pracownik("Henryk Hacker", 50000, "2003-10-01");
        personel[2] = new Pracownik("Antoni Tester", 40000, "2005-03-15");

        // zwieksz pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %S\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);

    }
}

class Pracownik
{
    public Pracownik(String nazwisko, double pobory, String dataZatrudnienia)
    {
        this.nazwisko = nazwisko;
        this.pobory = pobory;
        this.dataZatrudnienia=LocalDate.parse(dataZatrudnienia, formatter);

        id = nextId;
        ++nextId;
    }

    public String nazwisko()
    {
        return nazwisko;
    }

    public double pobory()
    {
        return pobory;
    }

    public String dataZatrudnienia()
    {
        return dataZatrudnienia.format(formatter);
    }

    public void zwiekszPobory(double procent)
    {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    public int id()
    {
        return id;
    }

    public void setId()
    {
        id = nextId;
        ++nextId;
    }

    public static int getNextId()
    {
        return nextId;
    }
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    private int id;
    private static int nextId = 1;
}
