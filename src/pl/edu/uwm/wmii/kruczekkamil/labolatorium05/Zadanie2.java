package pl.edu.uwm.wmii.kruczekkamil.laboratorium02;

public class Zadanie2 {
    public static void main(String[] args) {
        IntegerSet zbior1 = new IntegerSet();
        zbior1.insertElement(56);
        zbior1.insertElement(58);
        zbior1.insertElement(59);
        zbior1.insertElement(60);
        zbior1.insertElement(61);

        IntegerSet zbior2 = new IntegerSet();
        zbior2.insertElement(56);
        zbior2.insertElement(58);
        zbior2.insertElement(59);
        zbior2.insertElement(60);
        zbior2.insertElement(61);

        System.out.print(zbior1.toString());
        System.out.print(zbior1.equals(zbior2));
    }
}

class IntegerSet {

    public IntegerSet()
    {
        tablica = new boolean[100];
    }

    public void insertElement(int n)
    {
        this.tablica[n-1]=true;
    }

    public void deleteElement(int n)
    {
        this.tablica[n-1]=false;
    }

    public String toString()
    {
        String wynik="";
        for(int i=0; i<100; i++)
        {
            if (this.tablica[i]==true)
                wynik+=i+1+" ";
        }
        return wynik;
    }

    public boolean equals(IntegerSet zbior)
    {
        for(int i=0; i<100; i++)
        {
            if(this.tablica[i]!=zbior.tablica[i])
                return false;
        }
        return true;
    }

    public int [] union ()
    {
        int wynik[];
    }

    private boolean tablica[];
}