package pl.edu.uwm.wmii.kruczekkamil.labolatorium05;

public class Zadanie1 {
    public static void main(String[] args) {
       RachunekBankowy saver1 = new RachunekBankowy(2000);
       RachunekBankowy saver2 = new RachunekBankowy(3000);

       RachunekBankowy.setRocznaStopaProcentowa(0.04);

       saver1.obliczMiesieczneOdsetki();
       saver2.obliczMiesieczneOdsetki();

       System.out.println(saver1.getSaldo());
       System.out.println(saver2.getSaldo());

       RachunekBankowy.setRocznaStopaProcentowa(0.05);

       saver1.obliczMiesieczneOdsetki();
       saver2.obliczMiesieczneOdsetki();

       System.out.println(saver1.getSaldo());
       System.out.println(saver2.getSaldo());
    }
}

class RachunekBankowy {
    public void obliczMiesieczneOdsetki()
    {
        this.saldo+=(this.saldo * rocznaStopaProcentowa) / 12;
    }

    static void setRocznaStopaProcentowa(double nowa)
    {
        rocznaStopaProcentowa=nowa;
    }

    public RachunekBankowy(double saldo)
    {
        this.saldo=saldo;
    }

    public double getSaldo()
    {
        return this.saldo;
    }

    static double rocznaStopaProcentowa;
    private double saldo=0;
}