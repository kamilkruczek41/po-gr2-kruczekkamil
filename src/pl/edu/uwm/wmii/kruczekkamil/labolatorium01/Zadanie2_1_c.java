package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_c {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, wybrane=0;
        double liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++) {
            liczba = odczyt.nextDouble();
            if (Math.sqrt(liczba) % 2 == 0)
                wybrane++;
        }
        System.out.println("Kwadraty liczb przystych: " + wybrane);
    }

}