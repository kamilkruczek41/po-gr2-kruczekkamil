package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_b {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile,liczba, wybrane=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++) {
            liczba = odczyt.nextInt();
            if ((liczba % 3 == 0) && (liczba%5!=0))
                wybrane++;
        }
        System.out.println("Podzielne przez 3 i nie przez 5: " + wybrane);
    }

}