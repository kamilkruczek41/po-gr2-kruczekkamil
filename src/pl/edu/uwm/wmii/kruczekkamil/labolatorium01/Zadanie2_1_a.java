package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_a {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile,liczba, parzyste=0, nieparzyste=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++) {
            liczba = odczyt.nextInt();
            if (liczba % 2 == 0)
                parzyste++;
            else
                nieparzyste++;
        }
        System.out.println("Parzyste: " + parzyste + "\n" + "Nieparzyste: " + nieparzyste);
       }

    }