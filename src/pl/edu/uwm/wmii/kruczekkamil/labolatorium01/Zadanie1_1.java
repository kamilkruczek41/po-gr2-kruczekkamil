package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie1_1 {

    public static void main(String[] args) {
        // a)
        System.out.print("Ile liczb? ");
        int liczba,ile,suma=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            suma+=liczba;
        }
        System.out.println(suma);

        // b)
        System.out.print("Ile liczb? ");
        int iloczyn=1;
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            iloczyn*=liczba;
        }
        System.out.println(iloczyn);

        // c)
        System.out.print("Ile liczb? ");
        suma=0;
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            suma+=Math.abs(liczba);
        }
        System.out.println(suma);

        // d)
        System.out.print("Ile liczb? ");
        iloczyn=1;
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            iloczyn*=Math.sqrt(Math.abs(liczba));
        }
        System.out.println(iloczyn);

        // e)
        iloczyn=1;
        System.out.print("Ile liczb? ");
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            iloczyn*=Math.abs(liczba);
        }
        System.out.println(iloczyn);

        // f)
        suma=0;
        System.out.print("Ile liczb? ");
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            suma+=(liczba^2);
        }
        System.out.println(suma);

        // g)
        suma=0;
        iloczyn=1;
        System.out.print("Ile liczb? ");
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            suma+=liczba;
            iloczyn*=liczba;
        }
        System.out.println(suma + " oraz " + iloczyn);

    }
}
