package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_h {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, wybrane=0;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=1;i<=ile;i++)
        {
            liczba= odczyt.nextInt();
            if(Math.abs(liczba)<Math.pow(i,2))
                wybrane++;
        }

        System.out.println("Liczby, ktore spelniaja warunek: " + wybrane);
    }

}