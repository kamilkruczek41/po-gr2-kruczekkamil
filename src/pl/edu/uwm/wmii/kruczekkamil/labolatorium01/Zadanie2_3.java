package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_3 {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, zero=0, dod=0, uj=0;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++)
        {
            liczba= odczyt.nextInt();
            if(liczba>0)
                dod++;
            else if(liczba==0)
                zero++;
            else
                uj++;
        }

        System.out.println("Liczby dodatnie: " + dod);
        System.out.println("Liczby ujemne: " + uj);
        System.out.println("Zera: " + zero);
    }

}