package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_2 {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, suma=0;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++)
        {
            liczba= odczyt.nextInt();
            if(liczba>0)
                suma+=liczba;
        }

        System.out.println("Podwojona suma liczb: " + suma*2);
    }

}