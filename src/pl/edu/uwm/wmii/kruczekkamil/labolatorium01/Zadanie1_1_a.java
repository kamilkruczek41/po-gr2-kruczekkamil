package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie1_1_a {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int liczba, ile, suma = 0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for (int i = 0; i < ile; i++) {
            liczba = odczyt.nextInt();
            suma += liczba;
        }
        System.out.println(suma);
    }
}