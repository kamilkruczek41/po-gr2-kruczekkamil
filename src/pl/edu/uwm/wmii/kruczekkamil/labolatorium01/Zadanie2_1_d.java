package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_d {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, wybrane=0;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        int [] ciag = new int[ile];
        for(int i=0;i<ile;i++) {
            liczba = odczyt.nextInt();
            ciag[i]=liczba;
        }

        for(int i=0;i<ile;i++) {
            if ((i<ile-1) && (i>0) && (ciag[i]<(double)(ciag[i-1]+ciag[i+1]/2)))
                wybrane++;
        }
        System.out.println("Liczby spelniajace warunek: " + wybrane);
    }

}