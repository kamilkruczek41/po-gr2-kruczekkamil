package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1_e {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, wybrane=0, silnia=1;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=1;i<=ile;i++)
        {
            liczba= odczyt.nextInt();
            silnia*=i;
            if(Math.pow(2,i)<liczba && liczba<silnia)
                wybrane++;
        }

        System.out.println("Liczby spelniajace warunek 2^k < liczba < k!: " + wybrane);
    }

}