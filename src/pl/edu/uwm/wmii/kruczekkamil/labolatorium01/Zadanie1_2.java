package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie1_2 {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile,liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        int [] ciag = new int[ile];
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            ciag[i]=liczba;
        }

        for (int i=1;i<ile;i++)
        {
            System.out.print(ciag[i] + ", ");
        }
        System.out.print(ciag[0]);
    }
}