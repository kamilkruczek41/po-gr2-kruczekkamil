package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie1_1_h {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, silnia=1;
        double  liczba, suma=0;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for (int i=1;i<=ile;i++)
        {
            liczba=odczyt.nextInt();
            silnia*=i;
            suma+=((Math.pow(-1,i)*liczba)/silnia);
        }
        System.out.println(suma);
    }
}