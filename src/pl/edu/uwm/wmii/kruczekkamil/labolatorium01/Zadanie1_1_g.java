package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie1_1_g {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, liczba, suma=0, iloczyn=1;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for (int i=0;i<ile;i++)
        {
            liczba=odczyt.nextInt();
            suma+=liczba;
            iloczyn*=liczba;
        }
        System.out.println(suma + " oraz " + iloczyn);
    }
}