package pl.edu.uwm.wmii.kruczekkamil.laboratorium01;
import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) {
        System.out.print("Ile liczb? ");
        int ile, najmniejsza=0, najwieksza=0;
        int liczba;
        Scanner odczyt = new Scanner(System.in);
        ile = odczyt.nextInt();
        for(int i=0;i<ile;i++)
        {
            liczba= odczyt.nextInt();
            if(liczba<najmniejsza)
                najmniejsza=liczba;
            else if(liczba>najwieksza)
                najwieksza=liczba;
        }

        System.out.println("Najwieksza: " + najwieksza);
        System.out.println("Najmniejsza: " + najmniejsza);
    }

}