package pl.imiajd.Kruczek;

public class Adres{
    public Adres(String ulica, String numer_domu, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    
    public Adres(String ulica, String numer_domu,String numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    
    public void pokaz()
    {
        System.out.println(this.kod_pocztowy + " " + this.miasto);
        if(this.numer_mieszkania!=null)
            System.out.println(this.ulica + " " + this.numer_domu + "/" + this.numer_mieszkania);
        else
            System.out.println(this.ulica + " " + this.numer_domu);
    }
   
    public boolean przed(Adres adres)
    {
        if(this.kod_pocztowy.compareTo(adres.kod_pocztowy)<0)
            return true;
        else
            return false;
    }
    
    private String ulica;
    private String numer_domu;
    private String numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;
}

