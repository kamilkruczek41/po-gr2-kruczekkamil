package pl.imiajd.Kruczek;

public class Student extends Osoba
{
    public Student (String imiona, String nazwisko, String dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(imiona,nazwisko,dataUrodzenia,plec);
        this.kierunek=kierunek;
        this.sredniaOcen=sredniaOcen;
    }
   
    public String getOpis()
    {
        return "kierunek studiow: " + kierunek;
    }
    
    public double getSredniaOcen()
    {
        return sredniaOcen;
    }
    
    public void setSredniaOcen(double sredniaOcen)
    {
        this.sredniaOcen=sredniaOcen;
    }
    
    private double sredniaOcen;
    private String kierunek;
}