/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.imiajd.Kruczek;

/**
 *
 * @author 2017
 */
public class BetterRectangle extends java.awt.Rectangle {
    public BetterRectangle(int x, int y, int width, int height)
    {
        super(x,y,width,height);
    }
    
    public int GetArea()
    {
        return this.height * this.width;
    }
    
    public int GetPerimeter()
    {
        return this.height*2 + this.width*2; 
    }
}
