/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.imiajd.Kruczek;

/**
 *
 * @author 2017
 */
public class NazwanyPunkt extends Punkt {
    public NazwanyPunkt(int x, int y, String name)
    {
        super(x,y);
        this.name = name;
    }
    
    public void show()
    {
        System.out.println(name + ":<" + x() + ", " + y() + ">");
    }
    
    private String name;
}
