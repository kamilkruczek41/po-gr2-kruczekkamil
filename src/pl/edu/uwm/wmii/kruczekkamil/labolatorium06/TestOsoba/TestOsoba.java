package pl.edu.uwm.wmii.kruczekkamil.laboratorium06;

import pl.imiajd.Kruczek.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Pracownik pracownik1 = new Pracownik("Paweł", "Gaweł", "1980-09-07","2005-05-12", false, 7500);
        System.out.println(pracownik1.getOpis() + "\n" + pracownik1.getImiona() + " " + pracownik1.getNazwisko() + "\n");

        Student student1 = new Student("Jarosław","Labedz","1999-05-23",false,"Informatyka",4.23);
        System.out.println(student1.getOpis() + "\n" + student1.getImiona() + " " + student1.getNazwisko());
    }
}