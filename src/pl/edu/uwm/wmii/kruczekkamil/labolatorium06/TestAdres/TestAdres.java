package pl.edu.uwm.wmii.kruczekkamil.laboratorium06;

import pl.imiajd.Kruczek.*;

public class TestAdres
{
    public static void main(String[] args)
    {
        Adres adres1 = new Adres("Krowia", "39", "Prawiek", "17-100");
        adres1.pokaz();

        Adres adres2 = new Adres("Pawia", "39", "19", "Kartacze", "17-120");
        adres2.pokaz();

        System.out.println(adres2.przed(adres1));
        System.out.println(adres1.przed(adres2));
    }
}